//Dante Dias
//3/6/19
//This program uses a series of loops to record certain types of data
import java.util.Scanner; //importing scanner

public class Hw05 {
  //main method
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in); //initializing new scanner
    
    int courseNum = 0; //variables to be filled by user input
    String departmentName = "";
    int numTimesMet = 0;
    int classTime = 0;
    String instructorName = "";
    int numStudents = 0;
    
    System.out.println("Please enter a course number: "); //while loop that checks to see if the user inputs an int
    while (!myScanner.hasNextInt()){
      String junkword = myScanner.next();
    }
    courseNum = myScanner.nextInt();
    
    System.out.println("Please enter a department name: "); //while loop that checks to see if the user inputs an int
    while (!myScanner.hasNext()){
       int junkword1 = myScanner.nextInt();
    }
    departmentName = myScanner.next();
    
    System.out.println("Please enter the number of times met a week: "); //while loop that checks to see if the user inputs an int
    while (!myScanner.hasNextInt()){
       String junkword2 = myScanner.next();
    }
    numTimesMet = myScanner.nextInt();
    
    System.out.println("Please enter a class time: "); //while loop that checks to see if the user inputs an int
    while (!myScanner.hasNextInt()){
       String junkword3 = myScanner.next();
    }
    classTime = myScanner.nextInt();
    
    System.out.println("Please enter the instructor name: ");//while loop that checks to see if the user inputs an int
    while (!myScanner.hasNext()){
       int junkword4 = myScanner.nextInt();
    }
    instructorName = myScanner.next();
    
    System.out.println("Please enter the number of students: ");//while loop that checks to see if the user inputs an int
    while (!myScanner.hasNextInt()){
       String junkword5 = myScanner.next();
    }
    numStudents = myScanner.nextInt();
  }
}