//Dante Dias
//2/1/19
//CSE 002 lab 02
//This program records the amount of time and the rotations that the front wheel on a bike performs during 
//two separate trips. It then prints these values along with the distances of each trip. 
//Lastly, it prints the total number of miles traveled between both trips
public class Cyclometer {
  //main method required for every java program
  public static void main(String[] args) {
    //our input data
    int secsTrip1=480;   //the amount of time that the first trip took
    int secsTrip2=3220;  //the amount of time that the second trip took
    int countsTrip1=1561;  //The number of rotations that the front wheel performed during the first trip
		int countsTrip2=9037; //The number of rotations that the front wheel performed during the second trip
    
    //our intermediate variables and output data
    double wheelDiameter=27.0,  //The diameter of the front wheel
  	PI=3.14159, //the number pi
  	feetPerMile=5280,  //How many feet there are in a mile
  	inchesPerFoot=12,   //How many inches in a foot
  	secondsPerMinute=60;  //How many seconds in a minute
	  double distanceTrip1, distanceTrip2,totalDistance;  //Declaring three doubles for the distances of each trip and the total distance
    
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts."); //prints out how many minutes the first trip was and how many rotations the front wheel had
	  System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts."); //prints out how many minutes the second trip was and how many rotations the front wheel had
    
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; 
	  totalDistance=distanceTrip1+distanceTrip2;
    
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");
   
  } //end of main method
  
} //end of class