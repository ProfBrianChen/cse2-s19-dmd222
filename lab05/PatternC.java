//Dante Dias
//3/8/19
//

import java.util.Scanner;

public class PatternC {
  
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Please enter your desired amount of rows: ");
    int input = myScanner.nextInt();
    
    for(int numRows=input; numRows > 0; numRows--) {
      for(int x=input; x>0; x--) {
		  if(x>(input-(numRows-1))){
			  System.out.print(" ");
		  }
		  else {
        System.out.print(x);
		  }
      }
	  System.out.println("");
    }
  }
}