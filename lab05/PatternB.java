//Dante Dias
//3/8/19
//

import java.util.Scanner;

public class PatternB {
  
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Please enter your desired amount of rows: ");
	while (!myScanner.hasNextInt()){ //while loop detecting if user input is an integer
      String junkWord = myScanner.next();
    }
    int input = myScanner.nextInt();
    
    for(int numRows=input; numRows > 0; numRows--) {
      for(int x=1; x<numRows+1; x++) {
        System.out.print(x + " ");
      }
      System.out.println("");
    }
  }
}