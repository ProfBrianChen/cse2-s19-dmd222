//Dante Dias
//3/1/19
//This code records a user inputted integer and produces a "twist" of that length
import java.util.Scanner; //importing scanner

public class TwistGenerator {
  //main method
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner (System.in);
    
    int length =0; //declaring integer for length of twist
    System.out.println("Please enter an integer: "); //prompting the user
    while (!myScanner.hasNextInt()){ //while loop detecting if user input is an integer
      String junkWord = myScanner.next();
    }
    length = myScanner.nextInt(); //recording user input 
    int top = length; 
    int middle = length;
    int bottom = length;
    while (top>0){ //while loop that prints out the first line of the twist
      System.out.print("\\");
      top=top-1;
      if (top>0) {
        System.out.print(" ");
        top=top-1;
        if (top>0){
          System.out.print("/");
          top=top-1;
        }
        else {
          System.out.println(" ");
          break;
        }
      }
      else {
        System.out.println(" ");
        break;
      }
    }
   while (middle>0){ //while loop that prints out the middle line of the twist
      System.out.print(" ");
      middle=middle-1;
      if (middle>0) {
        System.out.print("X");
        middle=middle-1;
        if (middle>0){
          System.out.print(" ");
          middle=middle-1;
        }
        else {
          System.out.println(" ");
          break;
        }
      }
      else {
        System.out.println(" ");
        break;
      }
    }
    while (bottom>0){ //while loop that prints out the bottom line of the twist
      System.out.print("/");
      bottom=bottom-1;
      if (bottom>0) {
        System.out.print(" ");
        bottom=bottom-1;
        if (bottom>0){
          System.out.print("\\");
          bottom=bottom-1;
        }
        else {
          System.out.println(" ");
          break;
        }
      }
      else {
        System.out.println(" ");
        break;
      }
    }
  }
}