//Dante Dias
//2/15/19
//This code generates a random number then uses if and switch statements to identify the suit and identity of a card based on
//the number chosen

import java.lang.Math; //importing the random number generator

public class CardGenerator {
  //main method required by all programs
  public static void main(String[] args) {
    
    int randNum = (int) (Math.random()*52);//choosing a random number between 1 and 52
    String cardSuit = "";//initializing a string for the suit
    String cardIdentity = "";//initializing a string for the identity
    
    if (1<=randNum && randNum<=13)//if statement that detects if a card is a diamond
      cardSuit = "Diamonds";
      switch(randNum){ //switch statement that identifies the specific identity of the card
        case 1:
           cardIdentity = "Ace";
          break;
        case 2:
           cardIdentity = "Two";
          break;
        case 3:
           cardIdentity = "Three";
          break;
        case 4:
           cardIdentity = "Four";
          break;
        case 5:
           cardIdentity = "Five";
          break;
        case 6:
           cardIdentity = "Six";
          break;
        case 7:
           cardIdentity = "Seven";
          break;
        case 8:
           cardIdentity = "Eight";
          break;
        case 9:
           cardIdentity = "Nine";
          break;
        case 10:
           cardIdentity = "Ten";
          break;
        case 11:
           cardIdentity = "Jack";
          break;
        case 12:
           cardIdentity = "Queen";
          break;
        case 13:
           cardIdentity = "King";
          break;
      }
    if (14<=randNum && randNum<=26)//if statement that detects if a card is a club
      cardSuit = "Clubs";
      switch(randNum){ //switch statement that identifies the specific identity of the card
        case 14:
           cardIdentity = "Ace";
          break;
        case 15:
           cardIdentity = "Two";
          break;
        case 16:
           cardIdentity = "Three";
          break;
        case 17:
           cardIdentity = "Four";
          break;
        case 18:
           cardIdentity = "Five";
          break;
        case 19:
           cardIdentity = "Six";
          break;
        case 20:
           cardIdentity = "Seven";
          break;
        case 21:
           cardIdentity = "Eight";
          break;
        case 22:
           cardIdentity = "Nine";
          break;
        case 23:
           cardIdentity = "Ten";
          break;
        case 24:
           cardIdentity = "Jack";
          break;
        case 25:
           cardIdentity = "Queen";
          break;
        case 26:
           cardIdentity = "King";
          break;
      }
    if (27<=randNum && randNum<=39)//if statement that detects if a card is a heart
      cardSuit = "Hearts";
      switch(randNum){ //switch statement that identifies the specific identity of the card
        case 27:
           cardIdentity = "Ace";
          break;
        case 28:
           cardIdentity = "Two";
          break;
        case 29:
           cardIdentity = "Three";
          break;
        case 30:
           cardIdentity = "Four";
          break;
        case 31:
           cardIdentity = "Five";
          break;
        case 32:
           cardIdentity = "Six";
          break;
        case 33:
           cardIdentity = "Seven";
          break;
        case 34:
           cardIdentity = "Eight";
          break;
        case 35:
           cardIdentity = "Nine";
          break;
        case 36:
           cardIdentity = "Ten";
          break;
        case 37:
           cardIdentity = "Jack";
          break;
        case 38:
           cardIdentity = "Queen";
          break;
        case 39:
           cardIdentity = "King";
          break;
      }
    if (40<=randNum && randNum<=52)//if statement that detects if a card is a spade
      cardSuit = "Spades";
      switch(randNum){ //switch statement that identifies the specific identity of the card
        case 40:
           cardIdentity = "Ace";
          break;
        case 41:
           cardIdentity = "Two";
          break;
        case 42:
           cardIdentity = "Three";
          break;
        case 43:
           cardIdentity = "Four";
          break;
        case 44:
           cardIdentity = "Five";
          break;
        case 45:
           cardIdentity = "Six";
          break;
        case 46:
           cardIdentity = "Seven";
          break;
        case 47:
           cardIdentity = "Eight";
          break;
        case 48:
           cardIdentity = "Nine";
          break;
        case 49:
           cardIdentity = "Ten";
          break;
        case 50:
           cardIdentity = "Jack";
          break;
        case 51:
           cardIdentity = "Queen";
          break;
        case 52:
           cardIdentity = "King";
          break;
      }
      System.out.println("You chose the " + cardIdentity + " of " + cardSuit);
          
  }
          
}
