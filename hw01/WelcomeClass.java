public class WelcomeClass {
  
  public static void main(String args[]){
    //Prints out the ASCII design line by line
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println(" ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");  
    System.out.println("<-D--M--D--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
      
  }
  
}