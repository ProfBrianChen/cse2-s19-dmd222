//Dante Dias
//2/11/19
//This program records a user inputted amount of meters and converts it to inches 
import java.util.Scanner; //importing the scanner to record user input

public class Convert {
  //main method required by all programs
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner (System.in); //new scanner
    
    System.out.print("Enter the distance in meters: "); //prompting the user input
    
    double meters = myScanner.nextDouble(); //assigning a double with the user inputted value
    
    double inches = meters * 39.3701; //converting the meter value to inches
    
    System.out.println(meters + " meters is " + inches + " inches."); //printing the converted value
    
  }
}