//Dante Dias
//2/11/19
//This program records user inputted dimensions of a box and calculates the volume
import java.util.Scanner; //importing the scanner to record user input

public class BoxVolume {
  //main method required by all programs
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner (System.in); //new scanner
    
    System.out.print("The width of the box is: "); //prompting the user input
    
    double width = myScanner.nextDouble(); //assigning a double with the user inputted value
    
    System.out.print("The height of the box is: "); //prompting the user input
    
    double height = myScanner.nextDouble(); //assigning a double with the user inputted value

    System.out.print("The length of the box is: "); //prompting the user input
    
    double length = myScanner.nextDouble(); //assigning a double with the user inputted value
    
    double volume = width * height * length; //converting the meter value to inches
    
    System.out.println("The volume of the box is: " + volume); //printing the volume
    
  }
}