//Dante Dias
//2/5/19
//This program records the cost of a check, the tip percentage, and the amount of people at dinner,
//and calculates how much each person owes.
import java.util.Scanner; //imports the scanner to record user input

public class Check {
  //main method required for every program.
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner (System.in);
    //new instance of a scanner
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    //prompting the user for the cost of the check
    double checkCost = myScanner.nextDouble();//assigning the user inputted value to a double
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):"  );
    //prompting the user for the tip
    double tipPercent = myScanner.nextDouble();//assigning the tip to a double
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    
    System.out.print("Enter the number of people who went out to dinner: ");
    //prompting the user for the amount of people
    int numPeople = myScanner.nextInt();//assigning that number to an int

    double totalCost;//the total cost with tip
    double costPerPerson;//the cost per person
    int dollars,   //whole dollar amount of cost 
          dimes, pennies; //for storing digits
              //to the right of the decimal point 
              //for the cost$ 
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson;
    //get dimes amount, e.g., 
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2 
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
  }//end of main method
}//end of class
