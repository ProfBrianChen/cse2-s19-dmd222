//Dante Dias
//2/19/19
//This code chooses a random poker hand and identifies if you have a pair, two pair, or three of a kind.This

import java.lang.Math; //importing random number generator

public class PokerHandCheck {
  
  public static void main(String[] args) {
    
    int randNum1 = (int) (Math.random()*52);//random number generators to choose the identity and suit of the cards
    int randNum2 = (int) (Math.random()*52);
    int randNum3 = (int) (Math.random()*52);
    int randNum4 = (int) (Math.random()*52);
    int randNum5 = (int) (Math.random()*52);
    
    String cardSuit1 = "";//initializing a string for the suit
    String cardIdentity1 = "";//initializing a string for the identity
    String cardSuit2 = "";//initializing a string for the suit
    String cardIdentity2 = "";//initializing a string for the identity
    String cardSuit3 = "";//initializing a string for the suit
    String cardIdentity3 = "";//initializing a string for the identity
    String cardSuit4 = "";//initializing a string for the suit
    String cardIdentity4 = "";//initializing a string for the identity
    String cardSuit5 = "";//initializing a string for the suit
    String cardIdentity5 = "";//initializing a string for the identity
    
    if (1<=randNum1 && randNum1<=13)//if statement that detects if a card is a diamond
      cardSuit1 = "Diamonds";
      switch(randNum1){ //switch statement that identifies the specific identity of the card
        case 1:
           cardIdentity1 = "Ace";
          break;
        case 2:
           cardIdentity1 = "Two";
          break;
        case 3:
           cardIdentity1 = "Three";
          break;
        case 4:
           cardIdentity1 = "Four";
          break;
        case 5:
           cardIdentity1 = "Five";
          break;
        case 6:
           cardIdentity1 = "Six";
          break;
        case 7:
           cardIdentity1 = "Seven";
          break;
        case 8:
           cardIdentity1 = "Eight";
          break;
        case 9:
           cardIdentity1 = "Nine";
          break;
        case 10:
           cardIdentity1 = "Ten";
          break;
        case 11:
           cardIdentity1 = "Jack";
          break;
        case 12:
           cardIdentity1 = "Queen";
          break;
        case 13:
           cardIdentity1 = "King";
          break;
      }
    if (14<=randNum1 && randNum1<=26)//if statement that detects if a card is a club
      cardSuit1 = "Clubs";
      switch(randNum1){ //switch statement that identifies the specific identity of the card
        case 14:
           cardIdentity1 = "Ace";
          break;
        case 15:
           cardIdentity1 = "Two";
          break;
        case 16:
           cardIdentity1 = "Three";
          break;
        case 17:
           cardIdentity1 = "Four";
          break;
        case 18:
           cardIdentity1 = "Five";
          break;
        case 19:
           cardIdentity1 = "Six";
          break;
        case 20:
           cardIdentity1 = "Seven";
          break;
        case 21:
           cardIdentity1 = "Eight";
          break;
        case 22:
           cardIdentity1 = "Nine";
          break;
        case 23:
           cardIdentity1 = "Ten";
          break;
        case 24:
           cardIdentity1 = "Jack";
          break;
        case 25:
           cardIdentity1 = "Queen";
          break;
        case 26:
           cardIdentity1 = "King";
          break;
      }
    if (27<=randNum1 && randNum1<=39)//if statement that detects if a card is a heart
      cardSuit1 = "Hearts";
      switch(randNum1){ //switch statement that identifies the specific identity of the card
        case 27:
           cardIdentity1 = "Ace";
          break;
        case 28:
           cardIdentity1 = "Two";
          break;
        case 29:
           cardIdentity1 = "Three";
          break;
        case 30:
           cardIdentity1 = "Four";
          break;
        case 31:
           cardIdentity1 = "Five";
          break;
        case 32:
           cardIdentity1 = "Six";
          break;
        case 33:
           cardIdentity1 = "Seven";
          break;
        case 34:
           cardIdentity1 = "Eight";
          break;
        case 35:
           cardIdentity1 = "Nine";
          break;
        case 36:
           cardIdentity1 = "Ten";
          break;
        case 37:
           cardIdentity1 = "Jack";
          break;
        case 38:
           cardIdentity1 = "Queen";
          break;
        case 39:
           cardIdentity1 = "King";
          break;
      }
    if (40<=randNum1 && randNum1<=52)//if statement that detects if a card is a spade
      cardSuit1 = "Spades";
      switch(randNum1){ //switch statement that identifies the specific identity of the card
        case 40:
           cardIdentity1 = "Ace";
          break;
        case 41:
           cardIdentity1 = "Two";
          break;
        case 42:
           cardIdentity1 = "Three";
          break;
        case 43:
           cardIdentity1 = "Four";
          break;
        case 44:
           cardIdentity1 = "Five";
          break;
        case 45:
           cardIdentity1 = "Six";
          break;
        case 46:
           cardIdentity1 = "Seven";
          break;
        case 47:
           cardIdentity1 = "Eight";
          break;
        case 48:
           cardIdentity1 = "Nine";
          break;
        case 49:
           cardIdentity1 = "Ten";
          break;
        case 50:
           cardIdentity1 = "Jack";
          break;
        case 51:
           cardIdentity1 = "Queen";
          break;
        case 52:
           cardIdentity1 = "King";
          break;
      }
     if (1<=randNum2 && randNum2<=13)//if statement that detects if a card is a diamond
      cardSuit2 = "Diamonds";
      switch(randNum2){ //switch statement that identifies the specific identity of the card
        case 1:
           cardIdentity2 = "Ace";
          break;
        case 2:
           cardIdentity2 = "Two";
          break;
        case 3:
           cardIdentity2 = "Three";
          break;
        case 4:
           cardIdentity2 = "Four";
          break;
        case 5:
           cardIdentity2 = "Five";
          break;
        case 6:
           cardIdentity2 = "Six";
          break;
        case 7:
           cardIdentity2 = "Seven";
          break;
        case 8:
           cardIdentity2 = "Eight";
          break;
        case 9:
           cardIdentity2 = "Nine";
          break;
        case 10:
           cardIdentity2 = "Ten";
          break;
        case 11:
           cardIdentity2 = "Jack";
          break;
        case 12:
           cardIdentity2 = "Queen";
          break;
        case 13:
           cardIdentity2 = "King";
          break;
      }
    if (14<=randNum2 && randNum2<=26)//if statement that detects if a card is a club
      cardSuit2 = "Clubs";
      switch(randNum2){ //switch statement that identifies the specific identity of the card
        case 14:
           cardIdentity2 = "Ace";
          break;
        case 15:
           cardIdentity2 = "Two";
          break;
        case 16:
           cardIdentity2 = "Three";
          break;
        case 17:
           cardIdentity2 = "Four";
          break;
        case 18:
           cardIdentity2 = "Five";
          break;
        case 19:
           cardIdentity2 = "Six";
          break;
        case 20:
           cardIdentity2 = "Seven";
          break;
        case 21:
           cardIdentity2 = "Eight";
          break;
        case 22:
           cardIdentity2 = "Nine";
          break;
        case 23:
           cardIdentity2 = "Ten";
          break;
        case 24:
           cardIdentity2 = "Jack";
          break;
        case 25:
           cardIdentity2 = "Queen";
          break;
        case 26:
           cardIdentity2 = "King";
          break;
      }
    if (27<=randNum2 && randNum2<=39)//if statement that detects if a card is a heart
      cardSuit2 = "Hearts";
      switch(randNum2){ //switch statement that identifies the specific identity of the card
        case 27:
           cardIdentity2 = "Ace";
          break;
        case 28:
           cardIdentity2 = "Two";
          break;
        case 29:
           cardIdentity2 = "Three";
          break;
        case 30:
           cardIdentity2 = "Four";
          break;
        case 31:
           cardIdentity2 = "Five";
          break;
        case 32:
           cardIdentity2 = "Six";
          break;
        case 33:
           cardIdentity2 = "Seven";
          break;
        case 34:
           cardIdentity2 = "Eight";
          break;
        case 35:
           cardIdentity2 = "Nine";
          break;
        case 36:
           cardIdentity2 = "Ten";
          break;
        case 37:
           cardIdentity2 = "Jack";
          break;
        case 38:
           cardIdentity2 = "Queen";
          break;
        case 39:
           cardIdentity2 = "King";
          break;
      }
    if (40<=randNum2 && randNum2<=52)//if statement that detects if a card is a spade
      cardSuit2 = "Spades";
      switch(randNum2){ //switch statement that identifies the specific identity of the card
        case 40:
           cardIdentity2 = "Ace";
          break;
        case 41:
           cardIdentity2 = "Two";
          break;
        case 42:
           cardIdentity2 = "Three";
          break;
        case 43:
           cardIdentity2 = "Four";
          break;
        case 44:
           cardIdentity2 = "Five";
          break;
        case 45:
           cardIdentity2 = "Six";
          break;
        case 46:
           cardIdentity2 = "Seven";
          break;
        case 47:
           cardIdentity2 = "Eight";
          break;
        case 48:
           cardIdentity2 = "Nine";
          break;
        case 49:
           cardIdentity2 = "Ten";
          break;
        case 50:
           cardIdentity2 = "Jack";
          break;
        case 51:
           cardIdentity2 = "Queen";
          break;
        case 52:
           cardIdentity2 = "King";
          break;
      }
     if (1<=randNum3 && randNum3<=13)//if statement that detects if a card is a diamond
      cardSuit3 = "Diamonds";
      switch(randNum3){ //switch statement that identifies the specific identity of the card
        case 1:
           cardIdentity3 = "Ace";
          break;
        case 2:
           cardIdentity3 = "Two";
          break;
        case 3:
           cardIdentity3 = "Three";
          break;
        case 4:
           cardIdentity3 = "Four";
          break;
        case 5:
           cardIdentity3 = "Five";
          break;
        case 6:
           cardIdentity3 = "Six";
          break;
        case 7:
           cardIdentity3 = "Seven";
          break;
        case 8:
           cardIdentity3 = "Eight";
          break;
        case 9:
           cardIdentity3 = "Nine";
          break;
        case 10:
           cardIdentity3 = "Ten";
          break;
        case 11:
           cardIdentity3 = "Jack";
          break;
        case 12:
           cardIdentity3 = "Queen";
          break;
        case 13:
           cardIdentity3 = "King";
          break;
      }
    if (14<=randNum3 && randNum3<=26)//if statement that detects if a card is a club
      cardSuit3 = "Clubs";
      switch(randNum3){ //switch statement that identifies the specific identity of the card
        case 14:
           cardIdentity3 = "Ace";
          break;
        case 15:
           cardIdentity3 = "Two";
          break;
        case 16:
           cardIdentity3 = "Three";
          break;
        case 17:
           cardIdentity3 = "Four";
          break;
        case 18:
           cardIdentity3 = "Five";
          break;
        case 19:
           cardIdentity3 = "Six";
          break;
        case 20:
           cardIdentity3 = "Seven";
          break;
        case 21:
           cardIdentity3 = "Eight";
          break;
        case 22:
           cardIdentity3 = "Nine";
          break;
        case 23:
           cardIdentity3 = "Ten";
          break;
        case 24:
           cardIdentity3 = "Jack";
          break;
        case 25:
           cardIdentity3 = "Queen";
          break;
        case 26:
           cardIdentity3 = "King";
          break;
      }
    if (27<=randNum3 && randNum3<=39)//if statement that detects if a card is a heart
      cardSuit3 = "Hearts";
      switch(randNum3){ //switch statement that identifies the specific identity of the card
        case 27:
           cardIdentity3 = "Ace";
          break;
        case 28:
           cardIdentity3 = "Two";
          break;
        case 29:
           cardIdentity3 = "Three";
          break;
        case 30:
           cardIdentity3 = "Four";
          break;
        case 31:
           cardIdentity3 = "Five";
          break;
        case 32:
           cardIdentity3 = "Six";
          break;
        case 33:
           cardIdentity3 = "Seven";
          break;
        case 34:
           cardIdentity3 = "Eight";
          break;
        case 35:
           cardIdentity3 = "Nine";
          break;
        case 36:
           cardIdentity3 = "Ten";
          break;
        case 37:
           cardIdentity3 = "Jack";
          break;
        case 38:
           cardIdentity3 = "Queen";
          break;
        case 39:
           cardIdentity3 = "King";
          break;
      }
    if (40<=randNum3 && randNum3<=52)//if statement that detects if a card is a spade
      cardSuit3 = "Spades";
      switch(randNum3){ //switch statement that identifies the specific identity of the card
        case 40:
           cardIdentity3 = "Ace";
          break;
        case 41:
           cardIdentity3 = "Two";
          break;
        case 42:
           cardIdentity3 = "Three";
          break;
        case 43:
           cardIdentity3 = "Four";
          break;
        case 44:
           cardIdentity3 = "Five";
          break;
        case 45:
           cardIdentity3 = "Six";
          break;
        case 46:
           cardIdentity3 = "Seven";
          break;
        case 47:
           cardIdentity3 = "Eight";
          break;
        case 48:
           cardIdentity3 = "Nine";
          break;
        case 49:
           cardIdentity3 = "Ten";
          break;
        case 50:
           cardIdentity3 = "Jack";
          break;
        case 51:
           cardIdentity3 = "Queen";
          break;
        case 52:
           cardIdentity3 = "King";
          break;
      }
     if (1<=randNum4 && randNum4<=13)//if statement that detects if a card is a diamond
      cardSuit4 = "Diamonds";
      switch(randNum4){ //switch statement that identifies the specific identity of the card
        case 1:
           cardIdentity4 = "Ace";
          break;
        case 2:
           cardIdentity4 = "Two";
          break;
        case 3:
           cardIdentity4 = "Three";
          break;
        case 4:
           cardIdentity4 = "Four";
          break;
        case 5:
           cardIdentity4 = "Five";
          break;
        case 6:
           cardIdentity4 = "Six";
          break;
        case 7:
           cardIdentity4 = "Seven";
          break;
        case 8:
           cardIdentity4 = "Eight";
          break;
        case 9:
           cardIdentity4 = "Nine";
          break;
        case 10:
           cardIdentity4 = "Ten";
          break;
        case 11:
           cardIdentity4 = "Jack";
          break;
        case 12:
           cardIdentity4 = "Queen";
          break;
        case 13:
           cardIdentity4 = "King";
          break;
      }
    if (14<=randNum4 && randNum4<=26)//if statement that detects if a card is a club
      cardSuit4 = "Clubs";
      switch(randNum4){ //switch statement that identifies the specific identity of the card
        case 14:
           cardIdentity4 = "Ace";
          break;
        case 15:
           cardIdentity4 = "Two";
          break;
        case 16:
           cardIdentity4 = "Three";
          break;
        case 17:
           cardIdentity4 = "Four";
          break;
        case 18:
           cardIdentity4 = "Five";
          break;
        case 19:
           cardIdentity4 = "Six";
          break;
        case 20:
           cardIdentity4 = "Seven";
          break;
        case 21:
           cardIdentity4 = "Eight";
          break;
        case 22:
           cardIdentity4 = "Nine";
          break;
        case 23:
           cardIdentity4 = "Ten";
          break;
        case 24:
           cardIdentity4 = "Jack";
          break;
        case 25:
           cardIdentity4 = "Queen";
          break;
        case 26:
           cardIdentity4 = "King";
          break;
      }
    if (27<=randNum4 && randNum4<=39)//if statement that detects if a card is a heart
      cardSuit4 = "Hearts";
      switch(randNum4){ //switch statement that identifies the specific identity of the card
        case 27:
           cardIdentity4 = "Ace";
          break;
        case 28:
           cardIdentity4 = "Two";
          break;
        case 29:
           cardIdentity4 = "Three";
          break;
        case 30:
           cardIdentity4 = "Four";
          break;
        case 31:
           cardIdentity4 = "Five";
          break;
        case 32:
           cardIdentity4 = "Six";
          break;
        case 33:
           cardIdentity4 = "Seven";
          break;
        case 34:
           cardIdentity4 = "Eight";
          break;
        case 35:
           cardIdentity4 = "Nine";
          break;
        case 36:
           cardIdentity4 = "Ten";
          break;
        case 37:
           cardIdentity4 = "Jack";
          break;
        case 38:
           cardIdentity4 = "Queen";
          break;
        case 39:
           cardIdentity4 = "King";
          break;
      }
    if (40<=randNum4 && randNum4<=52)//if statement that detects if a card is a spade
      cardSuit4 = "Spades";
      switch(randNum4){ //switch statement that identifies the specific identity of the card
        case 40:
           cardIdentity4 = "Ace";
          break;
        case 41:
           cardIdentity4 = "Two";
          break;
        case 42:
           cardIdentity4 = "Three";
          break;
        case 43:
           cardIdentity4 = "Four";
          break;
        case 44:
           cardIdentity4 = "Five";
          break;
        case 45:
           cardIdentity4 = "Six";
          break;
        case 46:
           cardIdentity4 = "Seven";
          break;
        case 47:
           cardIdentity4 = "Eight";
          break;
        case 48:
           cardIdentity4 = "Nine";
          break;
        case 49:
           cardIdentity4 = "Ten";
          break;
        case 50:
           cardIdentity4 = "Jack";
          break;
        case 51:
           cardIdentity4 = "Queen";
          break;
        case 52:
           cardIdentity4 = "King";
          break;
      }
     if (1<=randNum5 && randNum5<=13)//if statement that detects if a card is a diamond
      cardSuit5 = "Diamonds";
      switch(randNum5){ //switch statement that identifies the specific identity of the card
        case 1:
           cardIdentity5 = "Ace";
          break;
        case 2:
           cardIdentity5 = "Two";
          break;
        case 3:
           cardIdentity5 = "Three";
          break;
        case 4:
           cardIdentity5 = "Four";
          break;
        case 5:
           cardIdentity5 = "Five";
          break;
        case 6:
           cardIdentity5 = "Six";
          break;
        case 7:
           cardIdentity5 = "Seven";
          break;
        case 8:
           cardIdentity5 = "Eight";
          break;
        case 9:
           cardIdentity5 = "Nine";
          break;
        case 10:
           cardIdentity5 = "Ten";
          break;
        case 11:
           cardIdentity5 = "Jack";
          break;
        case 12:
           cardIdentity5 = "Queen";
          break;
        case 13:
           cardIdentity5 = "King";
          break;
      }
    if (14<=randNum5 && randNum5<=26)//if statement that detects if a card is a club
      cardSuit5 = "Clubs";
      switch(randNum5){ //switch statement that identifies the specific identity of the card
        case 14:
           cardIdentity5 = "Ace";
          break;
        case 15:
           cardIdentity5 = "Two";
          break;
        case 16:
           cardIdentity5 = "Three";
          break;
        case 17:
           cardIdentity5 = "Four";
          break;
        case 18:
           cardIdentity5 = "Five";
          break;
        case 19:
           cardIdentity5 = "Six";
          break;
        case 20:
           cardIdentity5 = "Seven";
          break;
        case 21:
           cardIdentity5 = "Eight";
          break;
        case 22:
           cardIdentity5 = "Nine";
          break;
        case 23:
           cardIdentity5 = "Ten";
          break;
        case 24:
           cardIdentity5 = "Jack";
          break;
        case 25:
           cardIdentity5 = "Queen";
          break;
        case 26:
           cardIdentity5 = "King";
          break;
      }
    if (27<=randNum5 && randNum5<=39)//if statement that detects if a card is a heart
      cardSuit5 = "Hearts";
      switch(randNum5){ //switch statement that identifies the specific identity of the card
        case 27:
           cardIdentity5 = "Ace";
          break;
        case 28:
           cardIdentity5 = "Two";
          break;
        case 29:
           cardIdentity5 = "Three";
          break;
        case 30:
           cardIdentity5 = "Four";
          break;
        case 31:
           cardIdentity5 = "Five";
          break;
        case 32:
           cardIdentity5 = "Six";
          break;
        case 33:
           cardIdentity5 = "Seven";
          break;
        case 34:
           cardIdentity5 = "Eight";
          break;
        case 35:
           cardIdentity5 = "Nine";
          break;
        case 36:
           cardIdentity5 = "Ten";
          break;
        case 37:
           cardIdentity5 = "Jack";
          break;
        case 38:
           cardIdentity5 = "Queen";
          break;
        case 39:
           cardIdentity5 = "King";
          break;
      }
    if (40<=randNum5 && randNum5<=52)//if statement that detects if a card is a spade
      cardSuit5 = "Spades";
      switch(randNum5){ //switch statement that identifies the specific identity of the card
        case 40:
           cardIdentity5 = "Ace";
          break;
        case 41:
           cardIdentity5 = "Two";
          break;
        case 42:
           cardIdentity5 = "Three";
          break;
        case 43:
           cardIdentity5 = "Four";
          break;
        case 44:
           cardIdentity5 = "Five";
          break;
        case 45:
           cardIdentity5 = "Six";
          break;
        case 46:
           cardIdentity5 = "Seven";
          break;
        case 47:
           cardIdentity5 = "Eight";
          break;
        case 48:
           cardIdentity5 = "Nine";
          break;
        case 49:
           cardIdentity5 = "Ten";
          break;
        case 50:
           cardIdentity5 = "Jack";
          break;
        case 51:
           cardIdentity5 = "Queen";
          break;
        case 52:
           cardIdentity5 = "King";
          break;
      } 
    
      
    System.out.println("Your random cards were:");
    System.out.println("The "+cardIdentity1+" of "+cardSuit1);
    System.out.println("The "+cardIdentity2+" of "+cardSuit2);
    System.out.println("The "+cardIdentity3+" of "+cardSuit3);
    System.out.println("The "+cardIdentity4+" of "+cardSuit4);
    System.out.println("The "+cardIdentity5+" of "+cardSuit5);
if (cardIdentity1==cardIdentity2 || cardIdentity1==cardIdentity3 || cardIdentity1==cardIdentity4 || cardIdentity1==cardIdentity5 || cardIdentity2==cardIdentity3 || cardIdentity2==cardIdentity4 || cardIdentity2==cardIdentity5 || cardIdentity3==cardIdentity4 || cardIdentity3==cardIdentity5 || cardIdentity4==cardIdentity5) {
      System.out.println("You have a pair!");
    }
  }
}
