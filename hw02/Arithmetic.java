//Dante Dias
//2/5/19
//This program computes the cost of items bought in a store using simple calculations

public class Arithmetic {
  //main method
  public static void main(String[] args) {
    
    //Number of pairs of pants
  int numPants = 3;
  //Cost per pair of pants
  double pantsPrice = 34.98;

  //Number of sweatshirts
  int numShirts = 2;
  //Cost per shirt
  double shirtPrice = 24.99;

  //Number of belts
  int numBelts = 1;
  //cost per belt
  double beltPrice = 33.99;

  //the tax rate
  double paSalesTax = 0.06;

  double totalCostPants = numPants * pantsPrice; //total cost of pants
  double totalCostShirts = numShirts * shirtPrice; //total cost of shirts
  double totalCostBelts = numBelts * beltPrice; //total cost belts
  double shirtSalesTax = totalCostShirts * paSalesTax; //amount of taxes charged from the cost of shirts
  double pantsSalesTax = totalCostPants * paSalesTax; //amount of taxes charged from the cost of pants
  double beltsSalesTax = totalCostBelts * paSalesTax; //amount of taxes charged from the cost of belts
  double totalSalesTax = shirtSalesTax + beltsSalesTax + pantsSalesTax; //total taxes charged
  double grossCost = totalCostBelts + totalCostPants + totalCostShirts; //before tax
  double netCost = grossCost + totalSalesTax; //after tax
    
  System.out.println("Total cost of the pants:" + totalCostPants);
  System.out.println("Total cost of the shirts:"+ totalCostShirts);
  System.out.println("Total cost of the belts:"+ totalCostBelts);
  System.out.println("Total tax of the pants:"+ pantsSalesTax);
  System.out.println("Total tax of the shirts:"+ shirtSalesTax);  
  System.out.println("Total tax of the belts:"+ beltsSalesTax);
  System.out.println("Total cost before tax:"+ grossCost);  
  System.out.println("Total cost after tax:"+ netCost);  


  
  }
}